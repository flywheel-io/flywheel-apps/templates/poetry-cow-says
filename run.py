#!/usr/bin/env python
"""The run script."""

import logging
import sys

from flywheel_gear_toolkit import GearToolkitContext

# This design with a separate main and parser module
# allows the gear to be publishable and the main interfaces
# to it can then be imported in another project which could
# chain multiple gears together.
from fw_gear_cow_says.main import run
from fw_gear_cow_says.parser import parse_config

# The gear is split up into 2 main components.  The run.py
# file which is executed when the container runs.
# The run.py file then imports the rest of the gear as a
# module.


log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:
    """Parses config and run."""
    # Call the example_gear.parser.parse_config function
    # to extract the importent configuration from the
    # manifest
    animal, text = parse_config(context)

    # Pass the important configuration into the
    # example_gear.main.run function to execute
    # the main functionality of the gear
    e_code = run(animal, text)

    # Exit the python script (and thus the container)
    # With the exit code returned by
    # example_gear.main.run function
    sys.exit(e_code)


# Only execute if file is run as main, not when
# imported by another module
if __name__ == "__main__":  # pragma: no cover
    # Get access to gear config, inputs, and sdk
    # client if enabled.
    with GearToolkitContext() as gear_context:
        # Initialize logging, set logging level based
        # on `debug` configuration key in gear config.
        gear_context.init_logging()

        # Pass the gear context into main function
        # defined above.
        main(gear_context)
