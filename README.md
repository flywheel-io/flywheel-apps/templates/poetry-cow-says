# Cow-says Gear (cow-says)  

## Overview

### Summary 

The CowSays gear is an demo utility gear that makes animals speak. It uses as an example to illustrate how our skeleton
gear template (available at https://gitlab.com/flywheel-io/scientific-solutions/gears/templates/skeleton) can be used.

### Cite 

*License:* MIT

### Classification

*Category:* converter

*Gear Level:*

- [ ] Project
- [ ] Subject
- [ ] Session
- [X] Acquisition
- [ ] Analysis

----

[[_TOC_]]

----
### Inputs

- *text-input*
  - __Name__: text-file
  - __Type__: file
  - __Optional__: False
  - __Description__: Text file for the animal.

### Config

- *debug*
  - __Name__: debug
  - __Type__: boolean
  - __Description__: Log debug messages
  - __Default__: False

- *animal*
  - __Name__: animal
  - __Type__: string
  - __Description__: Type of animal
  - __Default__: cow

### Outputs

This gear will generate an image of the selected
animal along with the message from the input text file.

<!-- markdownlint-disable-file MD040 -->
```
  ______________
< Flywheel rocks >
  ==============
            \
             \
               ^__^                             
               (oo)\_______                   
               (__)\       )\/\             
                   ||----w |           
                   ||     ||  
```

## Usage

### Description

This gear takes an input text file and
print out the message in the text file along
with selected animal type in the gear log.

#### File Specifications

##### text-file

A text file that contains message that user wish to be printed on the gear log

### Workflow

A picture and description of the workflow

```mermaid
graph LR;
    A[Input-File]:::input --> C;
    C[Upload] --> D[Acquisition container];
    D:::container --> E((Gear));
    E:::gear --> F[Gear Log]:::container;
    
    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff

```

Description of workflow

1. Upload file to an Acquisition container
1. Select file as input to cow-says gear
1. View the outputs of the gear in Gear Log

## Contributing

[For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).]
<!-- markdownlint-disable-file -->