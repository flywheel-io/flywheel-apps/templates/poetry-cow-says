# Gear Development Guidelines

## Getting started

This document provides guidelines for developing Python-based Flywheel gears on Gitlab
using this template.

A few resources to get started:

* Gears are hosted at <https://gitlab.com/flywheel-io/scientific-solutions/gears/>.
* We are following gear specification v2.0 which is hosted at
  <https://gitlab.com/flywheel-io/public/gears/-/tree/master/spec>.
* This repository serves as a placeholder to document and illustrate these
  guidelines.

Flywheel-apps constitute an ever growing number of git repositories
(150+ as of Feb 2021) which presents a challenge. Enforcing common
practices around gear development improves maintainability, robustness, interoperability
and development efficacy. This document lays out a set of such good practices to follow.
__Good practices are also an ever evolving target and all developers are encouraged to
contribute to their definition.__  

Raise an issue here or merge request to discuss additions and changes.

## Code Structure

The following code organization definition achieves the following goals:

* Defines a clean split between the Flywheel interface (e.g. manifest.json, Dockerfile, run.py)
and the underlying algorithm / code source.
* Turns any gear in PyPi deployable package for easy re-usability and pipelining.
* Provides a standard naming convention structure to the code so that other developers in
  the team can easily get oriented.

Here is the structure of this repo:

```bash
├── fw_gear_cow_says        # Main repo for the package, deployable to PyPi,
│   ├── __init__.py         # containing your gear source code, etc.
│   ├── main.py           
│   └── parser.py           # A submodule containing the parser for config.json file
│
└── tests                   # Test folder hosting unit and other tests 
│   ├── test_main.py        # test module defining pytest functions
│   └── test_parser.py
│     
├── .dockerignore           # The .dockerignore file (reduce docker context build)
├── .gitignore              # The .gitignore file to ignore file from the git tree
├── .gitlab-ci.yml          # Gitlab CI template
├── .pre-commit-config.yml  # The pre-commit hooks config
├── CODEOWNERS              # Allows for defining the default reviewers
├── CONTRIBUTING.md         # Explain how to use the repo for developement
├── Dockerfile              # Dockerfile for the gear
├── LICENSE                 # License file, ususally MIT
├── README.md               # Explaining gear usage
├── manifest.json           # The gear manifest
├── poetry.lock             # Lock file for poetry
├── pyproject.toml          # The project TOML file
├── requiremnts-dev.txt     # The auto-generated dev dependencies file
├── requiremnts.txt         # The auto-generated dev dependencies file
└── run.py                  # Gear run.py file
```

And below a few principles to enforce whenever possible:

### run.py

* The `run.py` should be as simple as possible and be responsible for:
  * Instantiating the GearToolkitContext
  * Orchestrating the gear by:
    * Parsing the config.json (e.g. using utilities in `fw_gear_cow_says.parser`)
      to extract the relevant args and kwargs for the gear.
    * Doing the gear stuffs leveraging the main package (e.g.  `fw_gear_cow_says`)
    * Organizing the output
  
## naming convention

For a gear named `cow-says`, the following should be named accordingly:

Git repo name:  `cow-says`  
Docker image in manifest.json:  `flywheel/cow-says:version`  
Source folder: `fw_gear_cow_says`  
Pip package in pyproject.toml: `fw-gear-cow-says`

## manifest.json

The `manifest.json` follows
[gear specification v2.0](https://gitlab.com/flywheel-io/public/gears/-/tree/master/spec).

Additional requirements are defined below:

* A `debug` boolean option must be present in the `config` namespace. That option
  is used by the `GearToolkitContext` to update the logging level of the gear.

* The `custom.flywheel.suite` value must be one of the following:
  * Conversion
  * Curation
  * Quality Assurance
  * Utility  
  * Export
  * Report
  * Image Processing - Cardiac
  * Image Processing - Diffusion
  * Image Processing - Digital Pathology
  * Image Processing - Functional
  * Image Processing - Musculoskeletal
  * Image Processing - Other
  * Image Processing - Perfusion
  * Image Processing - Segmentation
  * Image Processing - Spectroscopy
  * Image Processing - Structural
  * Other
  
* The manifest.json must be alphabetically sorted (it will be by default using `pre-commit`
  hooks)
  
* In preparation for compute/gear v3, keyword lists should be defined in `custom.flywheel.classification`
  with the below categories (the example values correspond to a BIDS based fMRI-prep gear):
  * `type`: Type of the inputs (e.g. `[dicom]`)
  * `modality`: Modality of the input images (e.g. `[MR]`)
  * `classification`: Classification of the input images (e.g. `[fMRI]`)
  * `dataset`: Expected format of the input (e.g. `[BIDS]`)
  * `components`: Third party dependencies the gear relies on (e.g. `[ANTs, freesurfer]`)
  * `species`: Type of species the gear is applied to (e.g. `[Human]`)
  * `keywords`: A use defined list of keywords (`[Neuroimaging, fMRI, BOLD, pre-processing]`)

## README.md

The readme should adhere

## Other

### requirements-dev.txt and requirements.txt

This python requirements file are auto-generated by the `poetry_export` hook and should
not be modified manually

## Development environment

Refer to [CONTRIBUTING.md](CONTRIBUTING.md) to get to know how to set up your environment
for this repository.

In a nutshell, the development environment is based on:

* [poetry](https://python-poetry.org)
* [pre-commit](https://pre-commit.com)

### pyproject.toml

The `pyproject.toml` is the poetry equivalent of the `setup.py` where you define
your package dependencies, your dev dependencies and all the attributes of your
package.

### .gitlab-ci.yml

This YAML file defines the Gitlab CI steps.

Checkout the [qa-ci](https://gitlab.com/flywheel-io/tools/etc/qa-ci) source repository
for more details.

### .pre-commit-config.yml

This YAML file defines the configuration of the hooks for [`pre-commit`](https://pre-commit.com/).
`pre-commit` hooks are shell scripts that run by default upon each commit and are used
to sanity check the code (e.g. linting, testing, formatting, etc.).

Checkout the [qa-ci](https://gitlab.com/flywheel-io/tools/etc/qa-ci) source repository
for more details.
