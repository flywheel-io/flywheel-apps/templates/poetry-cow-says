from unittest.mock import MagicMock

import pytest
from flywheel_gear_toolkit import GearToolkitContext

import run


def test_main(mocker):
    parse_config = mocker.patch("run.parse_config")
    parse_config.return_value = ("animal", "some_text")

    mock_main_run = mocker.patch("run.run")
    mock_main_run.return_value = 0

    gear_context = MagicMock(spec=GearToolkitContext)

    with pytest.raises(SystemExit) as e_message:
        run.main(gear_context)

    assert e_message.type is SystemExit
    assert e_message.value.code == 0
    parse_config.assert_called_once_with(gear_context)
    mock_main_run.assert_called_once()
